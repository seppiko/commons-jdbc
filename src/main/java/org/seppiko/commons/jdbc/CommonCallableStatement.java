/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

/**
 * Common Callable Statement
 *
 * @author Leonard Woo
 */
public interface CommonCallableStatement extends CommonPreparedStatement, CallableStatement {

  /** Default SQl type. */
  int DEFAULT_SQL_TYPE = Types.VARCHAR;

  /**
   * convert parameter name to parameter index
   *
   * @param parameterName parameter name
   * @return parameter index
   */
  int findParameterIndexByName(String parameterName);

  /** {@inheritDoc} */
  @Override
  @Deprecated(since = "1.2")
  default BigDecimal getBigDecimal(int parameterIndex, int scale) throws SQLException {
    throw ExceptionFactory.unknown("this method is deprecated.");
  }

  /** {@inheritDoc} */
  @Override
  default Timestamp getTimestamp(int parameterIndex) throws SQLException {
    return getTimestamp(parameterIndex, null);
  }

  /** {@inheritDoc} */
  @Override
  default void registerOutParameter(String parameterName, int sqlType) throws SQLException {
    registerOutParameter(parameterName, sqlType, null);
  }

  /** {@inheritDoc} */
  @Override
  default void registerOutParameter(String parameterName, int sqlType, int scale)
      throws SQLException {
    registerOutParameter(findParameterIndexByName(parameterName), sqlType, scale);
  }

  /** {@inheritDoc} */
  @Override
  default void registerOutParameter(String parameterName, int sqlType, String typeName)
      throws SQLException {
    registerOutParameter(findParameterIndexByName(parameterName), sqlType, typeName);
  }

  /** {@inheritDoc} */
  @Override
  default void setURL(String parameterName, URL val) throws SQLException {
    setURL(findParameterIndexByName(parameterName), val);
  }

  /** {@inheritDoc} */
  @Override
  default void setNull(String parameterName, int sqlType) throws SQLException {
    setNull(parameterName, sqlType, null);
  }

  /** {@inheritDoc} */
  @Override
  default void setBoolean(String parameterName, boolean x) throws SQLException {
    setBoolean(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setByte(String parameterName, byte x) throws SQLException {
    setByte(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setShort(String parameterName, short x) throws SQLException {
    setShort(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setInt(String parameterName, int x) throws SQLException {
    setInt(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setLong(String parameterName, long x) throws SQLException {
    setLong(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setFloat(String parameterName, float x) throws SQLException {
    setFloat(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setDouble(String parameterName, double x) throws SQLException {
    setDouble(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setBigDecimal(String parameterName, BigDecimal x) throws SQLException {
    setBigDecimal(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setString(String parameterName, String x) throws SQLException {
    setString(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setBytes(String parameterName, byte[] x) throws SQLException {
    setBytes(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setDate(String parameterName, Date x) throws SQLException {
    setDate(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setTime(String parameterName, Time x) throws SQLException {
    setTime(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setTimestamp(String parameterName, Timestamp x) throws SQLException {
    setTimestamp(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setAsciiStream(String parameterName, InputStream x, int length) throws SQLException {
    setAsciiStream(parameterName, x, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setBinaryStream(String parameterName, InputStream x, int length)
      throws SQLException {
    setBinaryStream(parameterName, x, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setObject(String parameterName, Object x, int targetSqlType, int scale)
      throws SQLException {
    setObject(findParameterIndexByName(parameterName), x, targetSqlType, scale);
  }

  /** {@inheritDoc} */
  @Override
  default void setObject(String parameterName, Object x, int targetSqlType) throws SQLException {
    setObject(findParameterIndexByName(parameterName), x, targetSqlType);
  }

  /** {@inheritDoc} */
  @Override
  default void setObject(String parameterName, Object x) throws SQLException {
    setObject(parameterName, x, DEFAULT_SQL_TYPE);
  }

  /** {@inheritDoc} */
  @Override
  default void setCharacterStream(String parameterName, Reader reader, int length)
      throws SQLException {
    setCharacterStream(parameterName, reader, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setDate(String parameterName, Date x, Calendar cal) throws SQLException {
    setDate(findParameterIndexByName(parameterName), x, cal);
  }

  /** {@inheritDoc} */
  @Override
  default void setTime(String parameterName, Time x, Calendar cal) throws SQLException {
    setTime(findParameterIndexByName(parameterName), x, cal);
  }

  /** {@inheritDoc} */
  @Override
  default void setTimestamp(String parameterName, Timestamp x, Calendar cal) throws SQLException {
    setTimestamp(findParameterIndexByName(parameterName), x, cal);
  }

  /** {@inheritDoc} */
  @Override
  default void setNull(String parameterName, int sqlType, String typeName) throws SQLException {
    setNull(findParameterIndexByName(parameterName), sqlType, typeName);
  }

  /** {@inheritDoc} */
  @Override
  default String getString(String parameterName) throws SQLException {
    return getString(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default boolean getBoolean(String parameterName) throws SQLException {
    return getBoolean(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default byte getByte(String parameterName) throws SQLException {
    return getByte(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default short getShort(String parameterName) throws SQLException {
    return getShort(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default int getInt(String parameterName) throws SQLException {
    return getInt(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default long getLong(String parameterName) throws SQLException {
    return getLong(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default float getFloat(String parameterName) throws SQLException {
    return getFloat(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default double getDouble(String parameterName) throws SQLException {
    return getDouble(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default byte[] getBytes(String parameterName) throws SQLException {
    return getBytes(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Date getDate(String parameterName) throws SQLException {
    return getDate(parameterName, null);
  }

  /** {@inheritDoc} */
  @Override
  default Time getTime(String parameterName) throws SQLException {
    return getTime(parameterName, null);
  }

  /** {@inheritDoc} */
  @Override
  default Timestamp getTimestamp(String parameterName) throws SQLException {
    return getTimestamp(parameterName, null);
  }

  /** {@inheritDoc} */
  @Override
  default Object getObject(String parameterName) throws SQLException {
    return getObject(parameterName, Collections.emptyMap());
  }

  /** {@inheritDoc} */
  @Override
  default BigDecimal getBigDecimal(String parameterName) throws SQLException {
    return getBigDecimal(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Object getObject(String parameterName, Map<String, Class<?>> map) throws SQLException {
    return getObject(findParameterIndexByName(parameterName), map);
  }

  /** {@inheritDoc} */
  @Override
  default Ref getRef(String parameterName) throws SQLException {
    throw ExceptionFactory.notSupport("REF parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Blob getBlob(String parameterName) throws SQLException {
    return getBlob(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Clob getClob(String parameterName) throws SQLException {
    return getClob(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Array getArray(String parameterName) throws SQLException {
    throw ExceptionFactory.notSupport("Array parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Date getDate(String parameterName, Calendar cal) throws SQLException {
    return getDate(findParameterIndexByName(parameterName), cal);
  }

  /** {@inheritDoc} */
  @Override
  default Time getTime(String parameterName, Calendar cal) throws SQLException {
    return getTime(findParameterIndexByName(parameterName), cal);
  }

  /** {@inheritDoc} */
  @Override
  default Timestamp getTimestamp(String parameterName, Calendar cal) throws SQLException {
    return getTimestamp(findParameterIndexByName(parameterName), cal);
  }

  /** {@inheritDoc} */
  @Override
  default URL getURL(String parameterName) throws SQLException {
    return getURL(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default RowId getRowId(String parameterName) throws SQLException {
    throw ExceptionFactory.notSupport("RowId parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setRowId(String parameterName, RowId x) throws SQLException {
    throw ExceptionFactory.notSupport("RowId parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setNString(String parameterName, String value) throws SQLException {
    setNString(findParameterIndexByName(parameterName), value);
  }

  /** {@inheritDoc} */
  @Override
  default void setNCharacterStream(String parameterName, Reader value, long length)
      throws SQLException {
    setNCharacterStream(findParameterIndexByName(parameterName), value, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setNClob(String parameterName, NClob value) throws SQLException {
    setNClob(findParameterIndexByName(parameterName), value);
  }

  /** {@inheritDoc} */
  @Override
  default void setClob(String parameterName, Reader reader, long length) throws SQLException {
    setClob(findParameterIndexByName(parameterName), reader, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setBlob(String parameterName, InputStream inputStream, long length)
      throws SQLException {
    setBlob(findParameterIndexByName(parameterName), inputStream, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setNClob(String parameterName, Reader reader, long length) throws SQLException {
    setNClob(findParameterIndexByName(parameterName), reader, length);
  }

  /** {@inheritDoc} */
  @Override
  default NClob getNClob(String parameterName) throws SQLException {
    return getNClob(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
    throw ExceptionFactory.notSupport("SQLXML type is not supported");
  }

  /** {@inheritDoc} */
  default SQLXML getSQLXML(int parameterIndex) throws SQLException {
    throw ExceptionFactory.notSupport("SQLXML type is not supported");
  }

  /** {@inheritDoc} */
  @Override
  default SQLXML getSQLXML(String parameterName) throws SQLException {
    throw ExceptionFactory.notSupport("SQLXML type is not supported");
  }

  /** {@inheritDoc} */
  @Override
  default String getNString(String parameterName) throws SQLException {
    return getNString(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Reader getNCharacterStream(String parameterName) throws SQLException {
    return getNCharacterStream(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default Reader getCharacterStream(String parameterName) throws SQLException {
    return getCharacterStream(findParameterIndexByName(parameterName));
  }

  /** {@inheritDoc} */
  @Override
  default void setBlob(String parameterName, Blob x) throws SQLException {
    setBlob(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setClob(String parameterName, Clob x) throws SQLException {
    setClob(findParameterIndexByName(parameterName), x);
  }

  /** {@inheritDoc} */
  @Override
  default void setAsciiStream(String parameterName, InputStream x, long length)
      throws SQLException {
    setAsciiStream(findParameterIndexByName(parameterName), x, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setBinaryStream(String parameterName, InputStream x, long length)
      throws SQLException {
    setBinaryStream(findParameterIndexByName(parameterName), x, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setCharacterStream(String parameterName, Reader reader, long length)
      throws SQLException {
    setCharacterStream(findParameterIndexByName(parameterName), reader, length);
  }

  /** {@inheritDoc} */
  @Override
  default void setAsciiStream(String parameterName, InputStream x) throws SQLException {
    setAsciiStream(parameterName, x, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setBinaryStream(String parameterName, InputStream x) throws SQLException {
    setBinaryStream(parameterName, x, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setCharacterStream(String parameterName, Reader reader) throws SQLException {
    setCharacterStream(parameterName, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setNCharacterStream(String parameterName, Reader value) throws SQLException {
    setNCharacterStream(parameterName, value, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setClob(String parameterName, Reader reader) throws SQLException {
    setClob(parameterName, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setBlob(String parameterName, InputStream inputStream) throws SQLException {
    setBlob(parameterName, inputStream, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setNClob(String parameterName, Reader reader) throws SQLException {
    setNClob(parameterName, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default <T> T getObject(String parameterName, Class<T> type) throws SQLException {
    return getObject(findParameterIndexByName(parameterName), type);
  }

}
