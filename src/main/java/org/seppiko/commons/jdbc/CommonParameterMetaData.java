/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.ParameterMetaData;
import java.sql.SQLException;

/**
 * Common Parameter MetaData
 *
 * @author Leonard Woo
 */
public interface CommonParameterMetaData extends ParameterMetaData, CommonWrapper {

  /** {@inheritDoc} */
  @Override
  default int isNullable(int idx) throws SQLException {
    checkIndex(idx);
    return ParameterMetaData.parameterNullable;
  }

  /** {@inheritDoc} */
  @Override
  default int getParameterType(int idx) throws SQLException {
    checkIndex(idx);
    throw ExceptionFactory.notSupport("Getting parameter type metadata are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default String getParameterClassName(int idx) throws SQLException {
    checkIndex(idx);
    throw ExceptionFactory.notSupport("Unknown parameter metadata class name");
  }

  /** {@inheritDoc} */
  @Override
  default int getParameterMode(int idx) throws SQLException {
    checkIndex(idx);
    return ParameterMetaData.parameterModeIn;
  }

  /**
   * Parameter index checker
   *
   * @param index column index.
   * @throws SQLException if index less than zero, or other error.
   */
  void checkIndex(int index) throws SQLException;

}
