/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Common Database MetaData
 *
 * @author Leonard Woo
 */
public interface CommonDatabaseMetaData extends DatabaseMetaData, CommonWrapper {

  /** {@inheritDoc} */
  @Override
  default boolean isCatalogAtStart() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default int getJDBCMajorVersion() throws SQLException {
    return 4;
  }

  /** {@inheritDoc} */
  @Override
  default int getJDBCMinorVersion() throws SQLException {
    return 3;
  }

  /** {@inheritDoc} */
  @Override
  default String getDriverVersion() throws SQLException {
    return getDriverMajorVersion() + "." + getDriverMinorVersion();
  }

  /** {@inheritDoc} */
  @Override
  default int getSQLStateType() throws SQLException {
    return sqlStateSQL99;
  }

  /** {@inheritDoc} */
  @Override
  default RowIdLifetime getRowIdLifetime() throws SQLException {
    return RowIdLifetime.ROWID_UNSUPPORTED;
  }

  /** {@inheritDoc} */
  @Override
  default boolean dataDefinitionCausesTransactionCommit() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean dataDefinitionIgnoredInTransactions() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean allProceduresAreCallable() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean allTablesAreSelectable() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean autoCommitFailureClosesAllResultSets() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default int getMaxBinaryLiteralLength() throws SQLException {
    return Integer.MAX_VALUE;
  }

  /** {@inheritDoc} */
  @Override
  default int getMaxCharLiteralLength() throws SQLException {
    return Integer.MAX_VALUE;
  }

  /** {@inheritDoc} */
  @Override
  default int getMaxColumnsInSelect() throws SQLException {
    return Short.MAX_VALUE;
  }

  /** {@inheritDoc} */
  @Override
  default int getDefaultTransactionIsolation() throws SQLException {
    return Connection.TRANSACTION_REPEATABLE_READ;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsMixedCaseQuotedIdentifiers() throws SQLException {
    return supportsMixedCaseIdentifiers();
  }

  /** {@inheritDoc} */
  @Override
  default boolean storesUpperCaseQuotedIdentifiers() throws SQLException {
    return storesUpperCaseIdentifiers();
  }

  /** {@inheritDoc} */
  @Override
  default boolean storesLowerCaseQuotedIdentifiers() throws SQLException {
    return storesLowerCaseIdentifiers();
  }

  /** {@inheritDoc} */
  @Override
  default boolean storesMixedCaseQuotedIdentifiers() throws SQLException {
    return storesMixedCaseIdentifiers();
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsAlterTableWithAddColumn() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsAlterTableWithDropColumn() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsColumnAliasing() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean nullPlusNonNullIsNull() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean nullsAreSortedHigh() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean nullsAreSortedLow() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean nullsAreSortedAtStart() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean nullsAreSortedAtEnd() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsConvert() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsConvert(int fromType, int toType) throws SQLException {
    return switch (fromType) {
      case Types.TINYINT, Types.SMALLINT, Types.INTEGER, Types.BIGINT, Types.REAL, Types.FLOAT,
          Types.DECIMAL, Types.NUMERIC, Types.DOUBLE, Types.BIT, Types.BOOLEAN -> switch (toType) {
            case Types.TINYINT, Types.SMALLINT, Types.INTEGER, Types.BIGINT, Types.REAL,
                Types.FLOAT, Types.DECIMAL, Types.NUMERIC, Types.DOUBLE, Types.BIT, Types.BOOLEAN,
                Types.CHAR, Types.VARCHAR, Types.LONGVARCHAR, Types.BINARY, Types.VARBINARY,
                Types.LONGVARBINARY -> true;
            default -> false;
          };
      case Types.BLOB -> switch (toType) {
        case Types.BINARY, Types.VARBINARY, Types.LONGVARBINARY, Types.CHAR, Types.VARCHAR,
            Types.LONGVARCHAR, Types.TINYINT, Types.SMALLINT, Types.INTEGER, Types.BIGINT,
            Types.REAL, Types.FLOAT, Types.DECIMAL, Types.NUMERIC, Types.DOUBLE, Types.BIT,
            Types.BOOLEAN -> true;
        default -> false;
      };
      case Types.CHAR, Types.CLOB, Types.VARCHAR, Types.LONGVARCHAR, Types.BINARY, Types.VARBINARY,
          Types.LONGVARBINARY -> switch (toType) {
            case Types.BIT, Types.TINYINT, Types.SMALLINT, Types.INTEGER, Types.BIGINT, Types.FLOAT,
                Types.REAL, Types.DOUBLE, Types.NUMERIC, Types.DECIMAL, Types.CHAR, Types.VARCHAR,
                Types.LONGVARCHAR, Types.BINARY, Types.VARBINARY, Types.LONGVARBINARY, Types.DATE,
                Types.TIME, Types.TIMESTAMP, Types.BLOB, Types.CLOB, Types.BOOLEAN, Types.NCHAR,
                Types.LONGNVARCHAR, Types.NCLOB -> true;
            default -> false;
          };
      case Types.DATE -> switch (toType) {
        case Types.DATE, Types.CHAR, Types.VARCHAR, Types.LONGVARCHAR, Types.BINARY,
            Types.VARBINARY, Types.LONGVARBINARY -> true;
        default -> false;
      };
      case Types.TIME -> switch (toType) {
        case Types.TIME, Types.CHAR, Types.VARCHAR, Types.LONGVARCHAR, Types.BINARY,
            Types.VARBINARY, Types.LONGVARBINARY -> true;
        default -> false;
      };
      case Types.TIMESTAMP -> switch (toType) {
        case Types.TIMESTAMP, Types.CHAR, Types.VARCHAR, Types.LONGVARCHAR, Types.BINARY,
            Types.VARBINARY, Types.LONGVARBINARY, Types.TIME, Types.DATE -> true;
        default -> false;
      };
      default -> false;
    };
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsTableCorrelationNames() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsDifferentTableCorrelationNames() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsExpressionsInOrderBy() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOrderByUnrelated() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsGroupBy() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsGroupByUnrelated() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsGroupByBeyondSelect() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsLikeEscapeClause() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsMultipleResultSets() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsMultipleTransactions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsNonNullableColumns() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsMinimumSQLGrammar() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCoreSQLGrammar() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsExtendedSQLGrammar() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsANSI92EntryLevelSQL() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsANSI92IntermediateSQL() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsANSI92FullSQL() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsIntegrityEnhancementFacility() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOuterJoins() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsFullOuterJoins() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsLimitedOuterJoins() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSchemasInDataManipulation() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSchemasInProcedureCalls() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSchemasInTableDefinitions() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSchemasInIndexDefinitions() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSchemasInPrivilegeDefinitions() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCatalogsInDataManipulation() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCatalogsInProcedureCalls() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCatalogsInTableDefinitions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCatalogsInIndexDefinitions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCatalogsInPrivilegeDefinitions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsPositionedDelete() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsPositionedUpdate() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSelectForUpdate() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsStoredProcedures() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSubqueriesInComparisons() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSubqueriesInExists() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSubqueriesInIns() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSubqueriesInQuantifieds() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsCorrelatedSubqueries() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsUnion() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsUnionAll() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOpenCursorsAcrossCommit() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOpenCursorsAcrossRollback() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOpenStatementsAcrossCommit() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsOpenStatementsAcrossRollback() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsTransactions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsTransactionIsolationLevel(int level) throws SQLException {
    return switch (level) {
      case Connection.TRANSACTION_READ_UNCOMMITTED, Connection.TRANSACTION_READ_COMMITTED,
          Connection.TRANSACTION_REPEATABLE_READ, Connection.TRANSACTION_SERIALIZABLE ->
          true;
      default -> false;
    };
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsDataDefinitionAndDataManipulationTransactions() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsDataManipulationTransactionsOnly() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsResultSetType(int type) throws SQLException {
    return (type == ResultSet.TYPE_SCROLL_INSENSITIVE || type == ResultSet.TYPE_FORWARD_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsResultSetConcurrency(int type, int concurrency) throws SQLException {
    return (type == ResultSet.TYPE_SCROLL_INSENSITIVE || type == ResultSet.TYPE_FORWARD_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default boolean ownUpdatesAreVisible(int type) throws SQLException {
    return supportsResultSetType(type);
  }

  /** {@inheritDoc} */
  @Override
  default boolean ownDeletesAreVisible(int type) throws SQLException {
    return supportsResultSetType(type);
  }

  /** {@inheritDoc} */
  @Override
  default boolean ownInsertsAreVisible(int type) throws SQLException {
    return supportsResultSetType(type);
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsBatchUpdates() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsSavepoints() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsGetGeneratedKeys() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default int getResultSetHoldability() throws SQLException {
    return ResultSet.HOLD_CURSORS_OVER_COMMIT;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsResultSetHoldability(int holdability) throws SQLException {
    return holdability == ResultSet.HOLD_CURSORS_OVER_COMMIT;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsStatementPooling() throws SQLException {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsStoredFunctionsUsingCallSyntax() throws SQLException {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default boolean supportsRefCursors() throws SQLException {
    return false;
  }

}
