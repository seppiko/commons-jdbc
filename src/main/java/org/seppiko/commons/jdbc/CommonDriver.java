/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.Driver;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * Common Driver
 *
 * @author Leonard Woo
 */
public interface CommonDriver extends Driver {

  /** {@inheritDoc} */
  @Override
  default boolean jdbcCompliant() {
    return true;
  }

  /** {@inheritDoc} */
  @Override
  default Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }

}
