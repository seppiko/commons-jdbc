/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Common ResultSet
 *
 * @author Leonard Woo
 */
public interface CommonResultSet extends ResultSet, CommonWrapper, AutoCloseable {

  /** method deprecated message */
  String METHOD_DEPRECATED = "This method is deprecated";
  /** not updatable error message */
  String NOT_UPDATABLE_ERROR = "Updates are not supported when using ResultSet.CONCUR_READ_ONLY";
  /** some method are not supported message */
  String METHOD_NOT_SUPPORTED_WITH_CONCUR_READ_ONLY =
      " are not supported when using ResultSet.CONCUR_READ_ONLY";
  /** some are not supported message */
  String ARE_NOT_SUPPORTED = " are not supported";

  /** {@inheritDoc} */
  @Deprecated
  @Override
  default BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
    throw ExceptionFactory.unknown(METHOD_DEPRECATED);
  }

  /** {@inheritDoc} */
  @Deprecated
  @Override
  default InputStream getUnicodeStream(int columnIndex) throws SQLException {
    throw ExceptionFactory.unknown(METHOD_DEPRECATED);
  }

  /** {@inheritDoc} */
  @Deprecated
  @Override
  default BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
    throw ExceptionFactory.unknown(METHOD_DEPRECATED);
  }

  /** {@inheritDoc} */
  @Deprecated
  @Override
  default InputStream getUnicodeStream(String columnLabel) throws SQLException {
    throw ExceptionFactory.unknown(METHOD_DEPRECATED);
  }

  /** {@inheritDoc} */
  @Override
  default boolean getBoolean(String columnLabel) throws SQLException {
    return getBoolean(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default byte getByte(String columnLabel) throws SQLException {
    return getByte(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default byte[] getBytes(String columnLabel) throws SQLException {
    return getBytes(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default short getShort(String columnLabel) throws SQLException {
    return getShort(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default int getInt(String columnLabel) throws SQLException {
    return getInt(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default long getLong(String columnLabel) throws SQLException {
    return getLong(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default float getFloat(String columnLabel) throws SQLException {
    return getFloat(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default double getDouble(String columnLabel) throws SQLException {
    return getDouble(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default BigDecimal getBigDecimal(String columnLabel) throws SQLException {
    return getBigDecimal(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Object getObject(String columnLabel) throws SQLException {
    return getObject(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
    return getObject(findColumn(columnLabel), map);
  }

  /** {@inheritDoc} */
  @Override
  default <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
    return getObject(findColumn(columnLabel), type);
  }

  /** {@inheritDoc} */
  @Override
  default String getString(String columnLabel) throws SQLException {
    return getString(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default String getNString(String columnLabel) throws SQLException {
    return getNString(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default URL getURL(String columnLabel) throws SQLException {
    return getURL(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Date getDate(String columnLabel) throws SQLException {
    return getDate(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Date getDate(String columnLabel, Calendar cal) throws SQLException {
    return getDate(findColumn(columnLabel), cal);
  }

  /** {@inheritDoc} */
  @Override
  default Time getTime(String columnLabel) throws SQLException {
    return getTime(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Time getTime(String columnLabel, Calendar cal) throws SQLException {
    return getTime(findColumn(columnLabel), cal);
  }

  /** {@inheritDoc} */
  @Override
  default Timestamp getTimestamp(String columnLabel) throws SQLException {
    return getTimestamp(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
    return getTimestamp(findColumn(columnLabel), cal);
  }

  /** {@inheritDoc} */
  @Override
  default Blob getBlob(String columnLabel) throws SQLException {
    return getBlob(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Clob getClob(String columnLabel) throws SQLException {
    return getClob(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default NClob getNClob(String columnLabel) throws SQLException {
    return getNClob(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default InputStream getAsciiStream(String columnLabel) throws SQLException {
    return getAsciiStream(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default InputStream getBinaryStream(String columnLabel) throws SQLException {
    return getBinaryStream(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Reader getCharacterStream(String columnLabel) throws SQLException {
    return getCharacterStream(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Reader getNCharacterStream(String columnLabel) throws SQLException {
    return getNCharacterStream(findColumn(columnLabel));
  }

  /** {@inheritDoc} */
  @Override
  default Array getArray(String columnLabel) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getArray not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Array getArray(int columnIndex) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getArray not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateArray(int columnIndex, Array x) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateArray not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateArray(String columnLabel, Array x) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateArray not supported");
  }

  /** {@inheritDoc} */
  @Override
  default RowId getRowId(String columnLabel) throws SQLException {
    throw ExceptionFactory.notSupport("RowId not supported");
  }

  /** {@inheritDoc} */
  @Override
  default RowId getRowId(int columnIndex) throws SQLException {
    throw ExceptionFactory.notSupport("RowId are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateRowId(int columnIndex, RowId x) throws SQLException {
    throw ExceptionFactory.notSupport("RowId are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateRowId(String columnLabel, RowId x) throws SQLException {
    throw ExceptionFactory.notSupport("RowId are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default SQLXML getSQLXML(String columnLabel) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getSQLXML not supported");
  }

  /** {@inheritDoc} */
  @Override
  default SQLXML getSQLXML(int columnIndex) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getSQLXML not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateSQLXML not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateSQLXML not supported");
  }

  /** {@inheritDoc} */
  @Override
  default String getCursorName() throws SQLException {
    throw ExceptionFactory.notSupport("Cursors not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Ref getRef(int columnIndex) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getRef not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Ref getRef(String columnLabel) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.getRef not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateRef(int columnIndex, Ref x) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateRef not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateRef(String columnLabel, Ref x) throws SQLException {
    throw ExceptionFactory.notSupport("Method ResultSet.updateRef not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(int columnIndex, Object x, SQLType targetSqlType) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(String columnLabel, Object x, SQLType targetSqlType)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNull(int columnIndex) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBoolean(int columnIndex, boolean x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateByte(int columnIndex, byte x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateShort(int columnIndex, short x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateInt(int columnIndex, int x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateLong(int columnIndex, long x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateFloat(int columnIndex, float x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateDouble(int columnIndex, double x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateString(int columnIndex, String x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBytes(int columnIndex, byte[] x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateDate(int columnIndex, Date x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateTime(int columnIndex, Time x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(int columnIndex, Object x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNull(String columnLabel) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBoolean(String columnLabel, boolean x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateByte(String columnLabel, byte x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateShort(String columnLabel, short x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateInt(String columnLabel, int x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateLong(String columnLabel, long x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateFloat(String columnLabel, float x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateDouble(String columnLabel, double x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateString(String columnLabel, String x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNString(int columnIndex, String nString) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNString(String columnLabel, String nString) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBytes(String columnLabel, byte[] x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateDate(String columnLabel, Date x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateTime(String columnLabel, Time x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(String columnLabel, InputStream x, int length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(String columnLabel, InputStream x, int length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(String columnLabel, Reader reader, int length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateObject(String columnLabel, Object x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(int columnIndex, Blob x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(String columnLabel, Blob x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(int columnIndex, Clob x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(String columnLabel, Clob x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(int columnIndex, NClob nClob) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(String columnLabel, NClob nClob) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNCharacterStream(String columnLabel, Reader reader, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(String columnLabel, InputStream x, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(String columnLabel, InputStream x, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(String columnLabel, Reader reader, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(int columnIndex, InputStream inputStream, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(String columnLabel, InputStream inputStream, long length)
      throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(int columnIndex, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateClob(String columnLabel, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(int columnIndex, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void updateNClob(String columnLabel, Reader reader) throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void cancelRowUpdates() throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void insertRow() throws SQLException {
    throw ExceptionFactory.notSupport("insertRow" + METHOD_NOT_SUPPORTED_WITH_CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default void updateRow() throws SQLException {
    throw ExceptionFactory.notSupport("updateRow" + METHOD_NOT_SUPPORTED_WITH_CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default void deleteRow() throws SQLException {
    throw ExceptionFactory.notSupport("deleteRow" + METHOD_NOT_SUPPORTED_WITH_CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default void refreshRow() throws SQLException {
    throw ExceptionFactory.notSupport("refreshRow" + METHOD_NOT_SUPPORTED_WITH_CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default boolean rowInserted() throws SQLException {
    throw ExceptionFactory.notSupport("Detecting inserts" + ARE_NOT_SUPPORTED);
  }

  /** {@inheritDoc} */
  @Override
  default boolean rowUpdated() throws SQLException {
    throw ExceptionFactory.notSupport("Detecting row updates" + ARE_NOT_SUPPORTED);
  }

  /** {@inheritDoc} */
  @Override
  default boolean rowDeleted() throws SQLException {
    throw ExceptionFactory.notSupport("Row deletes" + ARE_NOT_SUPPORTED);
  }

  /** {@inheritDoc} */
  @Override
  default void moveToCurrentRow() throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

  /** {@inheritDoc} */
  @Override
  default void moveToInsertRow() throws SQLException {
    throw ExceptionFactory.notSupport(NOT_UPDATABLE_ERROR);
  }

}
