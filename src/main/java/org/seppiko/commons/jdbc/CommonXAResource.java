/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

/**
 * Common XA Resource
 *
 * @author Leonard Woo
 */
public interface CommonXAResource extends XAResource {

  /**
   * Flag number to name
   *
   * @param flags flag number
   * @return name
   * @see #start(Xid, int)
   * @see #end(Xid, int)
   */
  default String flagsToString(int flags) {
    return switch (flags) {
      case TMJOIN -> "JOIN";
      case TMONEPHASE -> "ONE PHASE";
      case TMRESUME -> "RESUME";
      case TMSUSPEND -> "SUSPEND";
      default -> "";
    };
  }

  /**
   * Convert SQL Exception to XA Exception.
   *
   * @param sqlEx SQLException object.
   * @return XAException object.
   */
  XAException mapXaException(SQLException sqlEx);

  /**
   * Execute a SQL Statement
   *
   * @param command SQL Statement
   * @throws XAException database access error occurs.
   */
  default void execute(String command) throws XAException {
    try {
      getConnection().createStatement().execute(command);
    } catch (SQLException ex) {
      throw mapXaException(ex);
    }
  }

  /**
   * Get pool connection
   *
   * @return connection
   * @throws SQLException access error
   */
  Connection getConnection() throws SQLException;

  /**
   * Create XID string
   *
   * @param xid XID value
   * @return XID string
   */
  String xidToString(Xid xid);

  /** {@inheritDoc} */
  @Override
  default void commit(Xid xid, boolean onePhase) throws XAException {
    execute("XA COMMIT " + xidToString(xid) + ((onePhase) ? " ONE PHASE" : ""));
  }

  /** {@inheritDoc} */
  @Override
  default int prepare(Xid xid) throws XAException {
    execute("XA PREPARE " + xidToString(xid));
    return XA_OK;
  }

  /** {@inheritDoc} */
  @Override
  default void rollback(Xid xid) throws XAException {
    execute("XA ROLLBACK " + xidToString(xid));
  }

  /** {@inheritDoc} */
  @Override
  default void start(Xid xid, int flags) throws XAException {
    if (flags != TMJOIN && flags != TMRESUME && flags != TMNOFLAGS) {
      throw new XAException(XAException.XAER_INVAL);
    }
    execute("XA START " + xidToString(xid) + " " + flagsToString(flags));
  }

  /** {@inheritDoc} */
  @Override
  default void end(Xid xid, int flags) throws XAException {
    if (flags != TMSUCCESS && flags != TMSUSPEND && flags != TMFAIL) {
      throw new XAException(XAException.XAER_INVAL);
    }

    execute("XA END " + xidToString(xid) + " " + flagsToString(flags));
  }

  /** {@inheritDoc} */
  @Override
  default int getTransactionTimeout() throws XAException {
    // Not implemented
    return 0;
  }

  /** {@inheritDoc} */
  @Override
  default boolean setTransactionTimeout(int seconds) throws XAException {
    return false;
  }

}
