/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Common Statement
 *
 * @author Leonard Woo
 */
public interface CommonStatement extends Statement, CommonWrapper, Cloneable {

  /** {@inheritDoc} */
  @Override
  default boolean execute(String sql, String[] columnNames) throws SQLException {
    checkNotClosed();
    return execute(sql, findColumnIndex(columnNames));
  }

  /** {@inheritDoc} */
  @Override
  default int executeUpdate(String sql, String[] columnNames) throws SQLException {
    checkNotClosed();
    return executeUpdate(sql, findColumnIndex(columnNames));
  }

  /** {@inheritDoc} */
  @Override
  default long executeLargeUpdate(String sql, String[] columnNames) throws SQLException {
    checkNotClosed();
    return executeLargeUpdate(sql, findColumnIndex(columnNames));
  }

  /** {@inheritDoc} */
  @Override
  default int getResultSetHoldability() throws SQLException {
    return ResultSet.HOLD_CURSORS_OVER_COMMIT;
  }

  /**
   * Connection checker.
   *
   * @throws SQLException if connection not closed.
   */
  default void checkNotClosed() throws SQLException {
    if (isClosed()) {
      throw ExceptionFactory.syntaxError("Can not do an operation on a closed statement");
    }
  }

  /**
   * Find column indexes.
   *
   * @param columnNames an array of the names of the columns that should be returned from the
   *     inserted row.
   * @return an array of the indexes of the columns in the inserted row that should be made
   *     available for retrieval by a call to the method {@code getGeneratedKeys}.
   * @throws SQLException column indexes not found.
   */
  int[] findColumnIndex(String[] columnNames) throws SQLException;

}
