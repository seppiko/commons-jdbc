/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.sql.Array;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Common Prepared Statement
 *
 * @author Leonard Woo
 */
public interface CommonPreparedStatement extends CommonStatement, PreparedStatement {

  /** {@inheritDoc} */
  @Deprecated
  @Override
  default void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
    throw ExceptionFactory.unknown("this method is deprecated.");
  }

  /** {@inheritDoc} */
  @Override
  default void setByte(int parameterIndex, byte x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setBytes(parameterIndex, new byte[] {x});
  }

  /** {@inheritDoc} */
  @Override
  default void setDate(int parameterIndex, Date x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setDate(parameterIndex, x, null);
  }

  /** {@inheritDoc} */
  @Override
  default void setTime(int parameterIndex, Time x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setTime(parameterIndex, x, null);
  }

  /** {@inheritDoc} */
  @Override
  default void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setTimestamp(parameterIndex, x, null);
  }

  /** {@inheritDoc} */
  @Override
  default void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setObject(parameterIndex, x, targetSqlType, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setObject(int parameterIndex, Object x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setObject(parameterIndex, x, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
    throw ExceptionFactory.notSupport("SQLXML type is not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setRef(int parameterIndex, Ref x) throws SQLException {
    throw ExceptionFactory.notSupport("REF parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setArray(int parameterIndex, Array x) throws SQLException {
    throw ExceptionFactory.notSupport("Array parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setRowId(int parameterIndex, RowId x) throws SQLException {
    throw ExceptionFactory.notSupport("RowId parameter are not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setClob(int parameterIndex, Reader reader) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setClob(parameterIndex, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setBlob(parameterIndex, inputStream, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setNClob(int parameterIndex, Reader reader) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setNClob(parameterIndex, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setAsciiStream(parameterIndex, x, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setAsciiStream(parameterIndex, x, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setBinaryStream(parameterIndex, x, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setBinaryStream(parameterIndex, x, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setCharacterStream(parameterIndex, reader, -1);
  }

  /** {@inheritDoc} */
  @Override
  default void setCharacterStream(int parameterIndex, Reader reader, int length)
      throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setCharacterStream(parameterIndex, reader, (long) length);
  }

  /** {@inheritDoc} */
  @Override
  default void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
    checkNotClosed();
    checkIndex(parameterIndex);
    setNCharacterStream(parameterIndex, value, -1);
  }

  /**
   * Parameter checker
   *
   * @param index column index.
   * @throws SQLException if index less than zero.
   */
  default void checkIndex(int index) throws SQLException {
    if (index <= 0) {
      throw ExceptionFactory.syntaxError(String.format("wrong parameter index %s", index));
    }
  }

}
