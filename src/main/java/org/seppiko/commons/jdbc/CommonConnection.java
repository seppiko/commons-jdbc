/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Struct;
import java.util.HashMap;
import java.util.Map;

/**
 * Common Connection
 *
 * @author Leonard Woo
 */
public interface CommonConnection extends Connection, CommonWrapper, AutoCloseable {

  /** {@inheritDoc} */
  @Override
  default Statement createStatement() throws SQLException {
    return createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default Statement createStatement(int resultSetType, int resultSetConcurrency)
      throws SQLException {
    return createStatement(resultSetType, resultSetConcurrency, getHoldability());
  }

  /** {@inheritDoc} */
  @Override
  default PreparedStatement prepareStatement(String sql) throws SQLException {
    return prepareStatement(sql, Statement.NO_GENERATED_KEYS);
  }

  /** {@inheritDoc} */
  @Override
  default PreparedStatement prepareStatement(
      String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
    return prepareStatement(sql, resultSetType, resultSetConcurrency, getHoldability());
  }

  /** {@inheritDoc} */
  @Override
  default CallableStatement prepareCall(String sql) throws SQLException {
    return prepareCall(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
  }

  /** {@inheritDoc} */
  @Override
  default CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
      throws SQLException {
    return prepareCall(sql, resultSetType, resultSetConcurrency, getHoldability());
  }

  /**
   * {@inheritDoc}
   *
   * @since 1.6
   */
  @Override
  default SQLXML createSQLXML() throws SQLException {
    throw ExceptionFactory.notSupport("SQLXML type is not supported");
  }

  /**
   * {@inheritDoc}
   *
   * @since 1.6
   */
  @Override
  default Array createArrayOf(String typeName, Object[] elements) throws SQLException {
    throw ExceptionFactory.notSupport("Array type is not supported");
  }

  /**
   * {@inheritDoc}
   *
   * @since 1.6
   */
  @Override
  default Struct createStruct(String typeName, Object[] attributes) throws SQLException {
    throw ExceptionFactory.notSupport("Struct type is not supported");
  }

  /** {@inheritDoc} */
  @Override
  default void setHoldability(int holdability) throws SQLException {
  }

  /** {@inheritDoc} */
  @Override
  default int getHoldability() throws SQLException {
    return ResultSet.HOLD_CURSORS_OVER_COMMIT;
  }

  /** {@inheritDoc} */
  @Override
  default void setTypeMap(Map<String, Class<?>> map) throws SQLException {
    throw ExceptionFactory.notSupport("setTypeMap is not supported");
  }

  /** {@inheritDoc} */
  @Override
  default Map<String, Class<?>> getTypeMap() throws SQLException {
    return new HashMap<>();
  }

}
