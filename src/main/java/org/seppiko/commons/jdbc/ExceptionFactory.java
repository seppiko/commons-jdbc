/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.jdbc;

import java.sql.SQLDataException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLSyntaxErrorException;
import java.sql.SQLTimeoutException;
import java.sql.SQLTransactionRollbackException;
import java.sql.SQLTransientConnectionException;

/**
 * JDBC Exception factory
 *
 * <p>You can use reflect change this sql state code, {@code XX50X} is default code.</p>
 * <p>The first two of {@code XX} are the values specified by JDBC.</p>
 *
 * @author Leonard Woo
 */
public class ExceptionFactory {

  private static final String DEFAULT_SQL_STATE_CODE = "NA500";
  private static final String SYNTAX_ERROR_SQL_STATE_CODE = "42501";
  private static final String NOT_SUPPORT_SQL_STATE_CODE = "0A502";
  private static final String TIMEOUT_SQL_STATE_CODE = "TO503";

  /**
   * Syntax Error Exception
   *
   * @param message exception message
   * @return SQLException
   */
  public static SQLException syntaxError(String message) {
    return new ExceptionFactory().create(message, SYNTAX_ERROR_SQL_STATE_CODE);
  }

  /**
   * Feature Not Supported Exception
   *
   * @param message exception message
   * @return SQLException
   */
  public static SQLException notSupport(String message) {
    return new ExceptionFactory().create(message, NOT_SUPPORT_SQL_STATE_CODE);
  }

  /**
   * Timeout Exception
   *
   * @param message exception message
   * @return SQLException
   */
  public static SQLException timeout(String message) {
    return new ExceptionFactory().create(message, TIMEOUT_SQL_STATE_CODE);
  }

  /**
   * SQL Exception
   *
   * @param message exception message
   * @return SQLException
   */
  public static SQLException unknown(String message) {
    return new ExceptionFactory().create(message);
  }

  /**
   * Exception factory default constructor.
   */
  public ExceptionFactory() {}

  /**
   * Create SQLException instance.
   *
   * @param message the exception message
   * @return SQLException instance
   */
  protected SQLException create(String message) {
    return create(message, DEFAULT_SQL_STATE_CODE);
  }

  /**
   * Create SQLException instance.
   *
   * @param message the exception message
   * @param sqlState an XOPEN or SQL:2003 code identifying the exception
   * @return SQLException instance
   */
  protected SQLException create(String message, String sqlState) {
    return createException(message, sqlState, -1, null);
  }

  /**
   * Create SQLException instance.
   *
   * @param message the exception message
   * @param sqlState an XOPEN or SQL:2003 code identifying the exception
   * @param errorCode a database vendor-specific exception code
   * @return SQLException instance
   */
  protected SQLException create(String message, String sqlState, int errorCode) {
    return createException(message, sqlState, errorCode, null);
  }

  /**
   * Create SQLException instance.
   *
   * @param message the exception message
   * @param sqlState an XOPEN or SQL:2003 code identifying the exception
   * @param cause the underlying reason for this {@code SQLException} (which is saved for later
   *     retrieval by the {@code getCause()} method); may be null indicating the cause is
   *     non-existent or unknown
   * @return SQLException instance
   */
  protected SQLException create(String message, String sqlState, Exception cause) {
    return createException(message, sqlState, -1, cause);
  }

  /**
   * Create SQLException instance.
   *
   * @param message the exception message
   * @param sqlState an XOPEN or SQL:2003 code identifying the exception
   * @param errorCode a database vendor-specific exception code
   * @param cause the underlying reason for this {@code SQLException} (which is saved for later
   *     retrieval by the {@code getCause()} method); may be null indicating the cause is
   *     non-existent or unknown.
   * @return SQLException instance
   */
  // JDBC Spec: 8.5 Categorized SQLExceptions
  private SQLException createException(
      String message, String sqlState, int errorCode, Exception cause) {

    String subClazz = (sqlState == null) ? "42" : sqlState.substring(0, 2);
    return switch (subClazz) {
      // Table 8-1 NonTransientSQLExeceptions Subclasses
      case "0A" -> new SQLFeatureNotSupportedException(message, sqlState, errorCode, cause);
      case "08" -> new SQLNonTransientConnectionException(message, sqlState, errorCode, cause);
      case "22" -> new SQLDataException(message, sqlState, errorCode, cause);
      case "23" ->
          new SQLIntegrityConstraintViolationException(message, sqlState, errorCode, cause);
      case "28" -> new SQLInvalidAuthorizationSpecException(message, sqlState, errorCode, cause);
      case "42" -> new SQLSyntaxErrorException(message, sqlState, errorCode, cause);

      // Table 8-2 NonTransientSQLExeceptions Subclasses
      case "TC" ->
          new SQLTransientConnectionException(message, sqlState, errorCode, cause); // 08 to TC
      case "40" -> new SQLTransactionRollbackException(message, sqlState, errorCode, cause);
      case "NA" -> new SQLException(message, sqlState, errorCode, cause);
      default -> new SQLTimeoutException(message, sqlState, errorCode, cause);
    };
  }
}
